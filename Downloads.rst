Downloads
----------

This document was moved to the GROMACS user manual. Please refer to the
`latest version <https://manual.gromacs.org/current/download.html>`_.

Older releases of the source code can be downloaded `here <https://manual.gromacs.org/documentation/>`_.


